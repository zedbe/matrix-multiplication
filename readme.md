# Matrix Multiplication
This is an exercise in python v3.6 without usage of numpy. Code includes validation of inputs, exception handling and basic test coverage.

### Nice to have for production
- retry on invalid input
- don't exit so brutal
- add logging and metrics

### How to run
- create venv (for example via pycharm)
- add pytest, mock... everything you need
- setup configuration & run src/mx_mul.py

### How to run tests
- from root run `python -m pytest tests/`

### Example
```bash
$ ./mx_mul.py
Matrix A
width: 2
height: 3

Matrix B
width: 1
height: 2

Matrix A values:
1 2
5 3
6 7

Matrix B values:
5
1

Result:
7
28
37
```
