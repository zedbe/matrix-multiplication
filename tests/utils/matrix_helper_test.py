import pytest

from src import Matrix, validate_shapes, create_empty_matrix, mx_mul, format_matrix


def test_matrix_helper_create_empty_matrix():
    """Given matrix width and height,
    when creating empty matrix,
    then create zero matrix with specific shape"""
    width = 2
    height = 3
    expected_matrix = [[0, 0],
                       [0, 0],
                       [0, 0]]

    assert create_empty_matrix(width, height) == expected_matrix


def test_matrix_helper_validate_shapes_failure():
    """Given 2 invalid matrix objects
    when validating their shapes shapes
    then exit with 100 code"""
    mat_a, mat_b = Matrix(3, 3), Matrix(2, 2)
    with pytest.raises(SystemExit) as wrapped_exit:
        validate_shapes(mat_a, mat_b)

    assert wrapped_exit.type == SystemExit
    assert wrapped_exit.value.code == 400


def test_matrix_helper_mx_mul():
    """Given 2 valid matrix objects
    when multiplication them
    then should be equal to expected result"""
    mat_a, mat_b = Matrix(3, 3), Matrix(2, 3)
    mat_a.content = [[1, 2, 3],
                     [4, 5, 6],
                     [7, 8, 9]]
    mat_b.content = [[8, 9],
                     [-5, 4],
                     [10, -1]]
    expected_matrix = [[28, 14],
                       [67, 50],
                       [106, 86]]

    assert mx_mul(mat_a, mat_b) == expected_matrix


def test_matrix_helper_format_matrix():
    """Given 2 valid matrix objects
    when formatting them
    then should be equal to expected result"""
    result_matrix = [[8, 9],
                     [-5, 4],
                     [10, -1]]
    expected_result = "8\t9\n" \
                      "-5\t4\n" \
                      "10\t-1\n"

    assert format_matrix(result_matrix) == expected_result
