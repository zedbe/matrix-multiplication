import pytest
import builtins
from unittest import mock
from src import Matrix


def test_matrix_validate_shape_fail():
    """Given matrix width and height,
    when initializing matrix object with invalid shape,
    then should throw 400 exit code"""
    width = 0
    height = 3
    with pytest.raises(SystemExit) as wrapped_exit:
        Matrix(width, height)

    assert wrapped_exit.type == SystemExit
    assert wrapped_exit.value.code == 400


def test_matrix_validate_shape_success():
    """Given matrix width and height,
    when initializing matrix object with valid shape,
    then should be create as expected"""
    width = 1
    height = 3
    init_matrix = Matrix(width, height)

    assert init_matrix.width == width
    assert init_matrix.height == height
    assert init_matrix.content == []


def test_matrix_fill_matrix_success():
    """Given matrix object
    when want to fill valid input to content
    then proceed successfully"""
    matrix = Matrix(3, 1)
    with mock.patch.object(builtins, 'input', lambda _: '1 2 3'):
        matrix.fill_matrix()

    assert matrix.content == [[1, 2, 3]]


def test_matrix_fill_matrix_fail_400():
    """Given matrix object
    when want to fill invalid input to content
    then fail proceed with 400 exit code"""
    matrix = Matrix(3, 1)
    with pytest.raises(SystemExit) as wrapped_exit:
        with mock.patch.object(builtins, 'input', lambda _: 'x x x'):
            matrix.fill_matrix()

    assert wrapped_exit.type == SystemExit
    assert wrapped_exit.value.code == 400


def test_matrix_validate_inputs_fail_400():
    """Given new row invalid candidate
    when want to fill to append to existing matrix
    then fail proceed with 400 exit code"""
    matrix = Matrix(3, 1)
    with pytest.raises(SystemExit) as wrapped_exit:
        matrix.validate_inputs([])

    assert wrapped_exit.type == SystemExit
    assert wrapped_exit.value.code == 400
