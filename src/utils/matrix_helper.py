#!/usr/bin/env python3


def create_empty_matrix(width, height):
    result = []
    for row in range(height):
        result.append([0] * width)
    return result


def validate_shapes(mat_a, mat_b):
    if mat_a.width != mat_b.height:
        print('Sorry, you entered wrong size of matrices :(')
        exit(400)
    else:
        pass


def mx_mul(mat_a, mat_b):
    result = create_empty_matrix(mat_b.width, mat_a.height)

    for row_from_A in range(len(mat_a.content)):
        for column_from_B in range(len(mat_b.content[0])):
            for coefficient in range(len(mat_b.content)):
                result[row_from_A][column_from_B] += \
                    mat_a.content[row_from_A][coefficient] * mat_b.content[coefficient][column_from_B]

    return result


def format_matrix(mat):
    formatted_matrix = ""
    for row in mat:
        formatted_matrix += '\t'.join(map(str, row))
        formatted_matrix += '\n'

    return formatted_matrix
