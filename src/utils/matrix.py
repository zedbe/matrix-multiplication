#!/usr/bin/env python3


def validate_shape(func):
    def inner(_, width, height):
        if width <= 0:
            print("I'm sorry, but Matrix width must be > 0.")
            exit(400)
        if height <= 0:
            print("I'm sorry, but Matrix height must be > 0.")
            exit(400)
        return func(_, width, height)

    return inner


class Matrix:
    @validate_shape
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.content = []

    def fill_matrix(self):
        for row in range(self.height):
            new_row = []

            try:
                new_row = list(map(int, input("").split()))
            except (UnicodeDecodeError, ValueError):
                print("You have entered wrong input. It must be an integer.")
                exit(400)
            except Exception as err:
                print(err)
                exit(500)

            self.validate_inputs(new_row)
            self.content.insert(row, new_row)

    def validate_inputs(self, new_row):
        if len(new_row) != self.width:
            print("You have entered wrong number of inputs per row. You should enter {0} but you did {1}"
                  .format(self.width, len(new_row)))
            exit(400)
