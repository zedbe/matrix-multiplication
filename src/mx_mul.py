#!/usr/bin/env python3
from src.utils import Matrix, validate_shapes, mx_mul, format_matrix

if __name__ == "__main__":
    try:
        print('Matrix A')
        mat_A = Matrix(int(input('width: ')), int(input('height: ')))
        print('\nMatrix B')
        mat_B = Matrix(int(input('width: ')), int(input('height: ')))
    except (UnicodeDecodeError, ValueError):
        print("I'm sorry, but this is invalid input.")
        exit(400)
    except Exception as err:
        print(err)
        exit(500)

    validate_shapes(mat_A, mat_B)

    print('\nMatrix A values:')
    mat_A.fill_matrix()
    print('\nMatrix B values:')
    mat_B.fill_matrix()

    result_matrix = mx_mul(mat_A, mat_B)

    print('\nResult:')
    print(format_matrix(result_matrix))
